FROM openjdk:8-jdk-alpine

# Add Maintainer Info
MAINTAINER Anon

# Add a volume pointing to /tmp
VOLUME /tmp

# The application's jar file
ARG JAR_FILE=target/demo-0.0.1-SNAPSHOT.jar
ADD ${JAR_FILE} sample.jar