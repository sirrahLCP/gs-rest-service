# AlpineLinux with a glibc-2.21 and Oracle Java 8

FROM alpine:3.8
MAINTAINER Anastas Dancha [...]

# Install cURL
RUN apk add curl &&\
apk add wget &&\ 
curl -Ls https://circle-artifacts.com/gh/andyshinn/alpine-pkg-glibc/6/artifacts/0/home/ubuntu/alpine-pkg-glibc/packages/x86_64/glibc-2.21-r2.apk > /tmp/glibc-2.21-r2.apk


# Java Version
ENV JAVA_VERSION_MAJOR 8
ENV JAVA_VERSION_MINOR 45
ENV JAVA_VERSION_BUILD 141
ENV JAVA_PACKAGE       jdk

# Download and unarchive Java
RUN mkdir /opt 
RUN wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u141-b15/336fa29ff2bb4ef291e347e091f7f4a7/jdk-8u141-linux-x64.tar.gz"
RUN tar -xzvf jdk-8u141-linux-x64.tar.gz -C /opt
RUN rm -rf /opt/jdk1.8.0_141/*src.zip \
           /opt/jdk1.8.0_141/lib/missioncontrol \
           /opt/jdk1.8.0_141/lib/visualvm \
           /opt/jdk1.8.0_141/lib/*javafx* \
           /opt/jdk1.8.0_141/jre/lib/plugin.jar \
           /opt/jdk1.8.0_141/jre/lib/ext/jfxrt.jar \
           /opt/jdk1.8.0_141/jre/bin/javaws \
           /opt/jdk1.8.0_141/jre/lib/javaws.jar \
           /opt/jdk1.8.0_141/jre/lib/desktop \
           /opt/jdk1.8.0_141/jre/plugin \
           /opt/jdk1.8.0_141/jre/lib/deploy* \
           /opt/jdk1.8.0_141/jre/lib/*javafx* \
           /opt/jdk1.8.0_141/jre/lib/*jfx* \
           /opt/jdk1.8.0_141/jre/lib/amd64/libdecora_sse.so \
           /opt/jdk1.8.0_141/jre/lib/amd64/libprism_*.so \
           /opt/jdk1.8.0_141/jre/lib/amd64/libfxplugins.so \
           /opt/jdk1.8.0_141/jre/lib/amd64/libglass.so \
           /opt/jdk1.8.0_141/jre/lib/amd64/libgstreamer-lite.so \
           /opt/jdk1.8.0_141/jre/lib/amd64/libjavafx*.so \
           /opt/jdk1.8.0_141/jre/lib/amd64/libjfx*.so

# Set environment
ENV JAVA_HOME /opt/jdk1.8.0_141
ENV PATH ${PATH}:${JAVA_HOME}/bin

# Add a volume pointing to /tmp
VOLUME /tmp

# The application's jar file
ARG JAR_FILE=target/demo-0.0.1-SNAPSHOT.jar

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/demo-0.0.1-SNAPSHOT.jar"]